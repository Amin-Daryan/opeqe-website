import React from 'react';
import { createMuiTheme, ThemeProvider, makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import ShoppingBasketIcon from '@material-ui/icons/ShoppingBasket';
import Button from '@material-ui/core/Button';
import { fade } from '@material-ui/core/styles/colorManipulator';

export default function Navbar({ scrollTop }) {
    const useStyles = makeStyles((theme) => ({
        signin: {
            '& > *': {
                borderRadius: "30px",
                fontSize: "12px",
                textTransform: 'none',
                padding: "5px 0px",
                boxSizing: "border-box",
                boxShadow: "none",
                border: scrollTop ? "1px solid #000000" : "1px solid rgb(2, 100, 95)",

                "&:hover": {
                    backgroundColor: scrollTop ? "transparent" : "#02645F",
                    boxShadow: "none"
                }
            },
        },
        signup: {
            '& > *': {
                borderRadius: "30px",
                fontSize: "12px",
                textTransform: 'none',
                padding: "5px 0px",
                boxSizing: "border-box",
                border: scrollTop ? "1px solid #000000" : "1px solid rgb(2, 100, 95)",
                boxShadow: "none",

                "&:hover": {
                    backgroundColor: scrollTop ? "#000000" : "#FFFFFF",
                    boxShadow: "none"
                }
            },
        },
    }));


    const theme = createMuiTheme({
        palette: {
            primary: {
                main: scrollTop ? fade("#02645F", 0) : "#02645F",
            },
            secondary: {
                main: scrollTop ? "#000000" : "#FFFFFF",
            },
        },
    });

    const classes = useStyles();


    return (
        <>
            <div className="navbar-container__image-container">
                <img src="https://cdn.opeqe.com/image/Logo/opeqe-logo.svg" alt="No image" />
            </div>
            <div className="navbar-container__buttons">
                <ThemeProvider theme={theme}>
                    <div className="navbar-container__buttons hover-black responsive-none">Reservation</div>
                    <div className="navbar-container__buttons hover-black  responsive-none">Orders</div>
                    <div className="navbar-container__buttons hover-black  responsive-none">Locations</div>
                    <div className={classes.signin}>
                        <Button variant="contained" color="primary">
                            <span style={{
                                color: scrollTop ? "#000000" : "#FFFFFF",
                                fontWeight: "500",
                            }}>Log in</span>
                        </Button>
                    </div>
                    <div className={classes.signup}>
                        <Button variant="contained" color="secondary" >
                            <span style={{
                                color: scrollTop ? "rgb(255, 210, 0)" : "rgb(2, 100, 95)",
                                fontWeight: "500",
                            }}>Sign up</span>
                        </Button></div>
                    <div>
                        <IconButton aria-label="" color="primary">
                            <ShoppingBasketIcon style={{ color: scrollTop ? "#000000" : "#02645F" }} />
                        </IconButton>
                    </div>
                </ThemeProvider>
            </div>
        </>
    )
}
