import React from "react";

export default function SearchBox() {
  return (
    <div className="flex">
      <div className="body-container__content__search-box__text-container flex-center mr-4">
        <span>ASAP Pickup</span>
        <span>Opeqe San Francisco - 235 Montgomery Street</span>
      </div>
      <div
        v
        className="body-container__content__search-box__button-container mr-4"
      >
        <button>Change</button>
      </div>
      <div className=" body-container__content__search-box__type-select flex-center mr-2">
        <div className="cursor-pointer">Delivery </div>
        <div className="seperator">or</div>
        <div className="cursor-pointer">Pickup</div>
      </div>
    </div>
  );
}
