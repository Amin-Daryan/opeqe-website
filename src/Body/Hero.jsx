import React from "react";

export default function Hero() {
  return (
    <>
      <img src="https://www.pngmart.com/files/1/Pepperoni-Pizza.png" alt="" />
      <div className="body-container__hero__image-container__header">
        <span>Happy Hour</span>
        <span>Get Unlimited %40 Off on Egg Burger with Trufle Mayo</span>
      </div>
      <div className="body-container__hero__image-container__button">
        <button>
          <span>Use</span>
          <span>Happy Hour</span>
        </button>
      </div>
    </>
  );
}
