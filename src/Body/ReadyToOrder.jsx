import React from "react";
import SearchBox from "./SearchBox";

export default function ReadyToOrder() {
  return (
    <>
      <div style={{ fontSize: "25px" }}>Ready to order?</div>
      <div style={{ fontSize: "18px", marginTop: "10px" }}>
        Browse our menu for dine-in, delivery or pickup and catering
      </div>
      <div className="flex-center py-2">
        <SearchBox />
      </div>
    </>
  );
}
