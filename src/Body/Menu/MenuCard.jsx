import React from "react";
import TimerIcon from "@material-ui/icons/Timer";

export default function MenuCard({ card }) {
  return (
    <div className="card-container">
      <div className="card-container__image-container">
        <img src={card.img} alt="" />
      </div>
      <div className="card-container__text mt-3">
        <div className="card-container__text__header">{card.name}</div>
        <div style={{ color: "#026764", fontWeight: "600" }}>
          {card.restaurant}
        </div>
        <div className="card-container__text__categories flex">
          {card.categories.map((category) => {
            return <div>{category}</div>;
          })}
        </div>
        <div className="card-container__text__footer  flex justify-content-between">
          <div className="card-container__text__footer__price-duration">
            <span className="mr-1">
              <TimerIcon style={{ fontSize: 15 }} />
              {card.duration} Mins
            </span>
            <span>${card.price}</span>
          </div>
          <div className="card-container__text__footer__free-pickup">
            {card.freePickup ? "Free Pickup" : ""}
          </div>
        </div>
      </div>
    </div>
  );
}
