import React, { useState, useEffect } from "react";
import MenuCard from "./MenuCard";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import { Swipeable } from 'react-swipeable';
import useMediaQuery from '@material-ui/core/useMediaQuery';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#02645F",
    },
  },
});

export default function Menu() {
  const menuData = [
    {
      name: "Happy Hour",
      items: [
        {
          name: "Quarter Dark and Leg",
          restaurant: "Fastfood",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "40-60",
          price: "16.67",
          freePickup: true,
          happyHoure: true,
        },
      ],
    },
    {
      name: "Lunch & Dinner",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Mexican",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Japanese",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Pizza",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Sandwich",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Breakfast",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Salad",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
    {
      name: "Soup",
      items: [
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/11.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/12.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
        {
          name: "T-Bone Steak & Eggs",
          restaurant: "A'la Carte",
          categories: ["AmericanMain", "Course", "Lunch & Dinner"],
          img: "https://cdn.opeqe.com/image/menu/s/3.jpg",
          duration: "4-6 ",
          price: "16.99",
          freePickup: true,
          happyHoure: false,
        },
      ],
    },
  ];
  const matches576px = useMediaQuery('(max-width:576px)');
  const matches1800px = useMediaQuery('(max-width:1800px)');
  const [arrowClass, setarrowClass] = useState([]);
  const [translateCarousel, setTranslateCarousel] = useState([]);
  const distance = matches576px ? 450 : matches1800px ? 2 * 555 : 3 * 555;

  function hanldeAppearArrow(index) {
    const newArrowClass = arrowClass.filter((item) => item.id !== index);
    setarrowClass([...newArrowClass, { type: "start", id: index }]);
  }

  function hanldeFadeArrow(index) {
    const newArrowClass = arrowClass.filter((item) => item.id !== index);
    setarrowClass([...newArrowClass, { type: "finish", id: index }]);
  }

  function handleTransition(index, direction, item) {
    if (translateCarousel.filter((item) => item.id === index)[0]
      ?.translate === 0 && direction === "left") return
    if (translateCarousel.filter((item) => item.id === index)[0]
      ?.translate <=
      -(item.items.length - (matches576px ? 2 : matches1800px ? 2 : 3)) * 555 && direction === "right") return
    const newTranslateCarousel = translateCarousel.filter(
      (item) => item.id !== index
    );
    setTranslateCarousel([
      ...newTranslateCarousel,
      {
        translate:
          (translateCarousel.filter((item) => item.id === index)[0]?.translate
            ? translateCarousel.filter((item) => item.id === index)[0]
              ?.translate
            : 0) - (direction === "right" ? distance : -distance),
        id: index,
      },
    ]);
  }

  useEffect(() => {
    let newTranslateCarousel = menuData.map((item, index) => {
      return {
        translate: 0,
        id: index,
      };
    });

    setTranslateCarousel(newTranslateCarousel);
  }, []);

  return menuData.map((item, index) => {
    return (
      <div
        className="body-container__content__menu__carousel-container"
        key={index}
      >
        <div className="title">{item.name}</div>
        <hr />
        <Swipeable
          onSwipedLeft={(eventData) => handleTransition(index, "right", item)}
          onSwipedRight={(eventData) => handleTransition(index, "left", item)} >
          <div
            className="body-container__content__menu__carousel-container__carousel flex"
            style={{
              width: `${item.items.length * 555}px`,
              transform: `translateX(${
                translateCarousel.filter((item) => item.id === index)[0]
                  ?.translate
                }px)`,
            }}
            onMouseOver={() => hanldeAppearArrow(index)}
            onMouseLeave={() => hanldeFadeArrow(index)}
          >
            {item.items.map((card) => {
              return <MenuCard card={card} />;
            })}
          </div>
        </Swipeable>

        <div
          className={`carousel-arrow right ${
            arrowClass.filter((item) => item.id === index)[0]?.type
            }`}
          style={{
            display:
              translateCarousel.filter((item) => item.id === index)[0]
                ?.translate <=
                -(item.items.length - (matches576px ? 2 : matches1800px ? 2 : 3)) * 555
                ? "none"
                : "",
          }}
          onMouseOver={() => hanldeAppearArrow(index)}
          onClick={() => handleTransition(index, "right", item)}
        >
          <ThemeProvider theme={theme}>
            <IconButton aria-label="" color="primary">
              <ChevronRightIcon />
            </IconButton>
          </ThemeProvider>
        </div>
        <div
          className={`carousel-arrow left ${
            arrowClass.filter((item) => item.id === index)[0]?.type
            }`}
          style={{
            display:
              translateCarousel.filter((item) => item.id === index)[0]
                ?.translate === 0
                ? "none"
                : "",
          }}
          onMouseOver={() => hanldeAppearArrow(index)}
          onClick={() => handleTransition(index, "left", item)}
        >
          <ThemeProvider theme={theme}>
            <IconButton aria-label="" color="primary">
              <ChevronLeftIcon />
            </IconButton>
          </ThemeProvider>
        </div>
      </div>
    );
  });
}
