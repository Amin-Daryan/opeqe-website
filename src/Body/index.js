import React from 'react';
import Hero from "./Hero";
import SearchBox from "./SearchBox";
import Menu from "./Menu";
import ReadyToOrder from "./ReadyToOrder";

export default function index() {
    return (
        <>
            <div className="body-container__hero">
                <div className="body-container__hero__image-container">
                    <Hero />
                </div>
            </div>
            <div className="body-container__content ">
                <div id="search-bar" className="body-container__content__search-box">
                    <SearchBox />
                </div>
                <div id="menu" className="body-container__content__menu">
                    <Menu />
                </div>
                <hr />
                <div id="ready-to-order" className="body-container__content__footer pt-3" style={{paddingBottom: "100px"}}>
                    <ReadyToOrder />
                </div>
            </div>
        </>
    )
}
