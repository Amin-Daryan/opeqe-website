import React from 'react';
import FooterIcons from "./FooterIcons";
import FooterMenu from "./FooterMenu";
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';

export default function Footer() {
    return (
        <>
            <div className="footer-container__icons-container flex-center">
                <FooterIcons />
            </div>
            <div className="footer-container__header flex">
                <a>About</a>
                <a>Services</a>
                <a>Support</a>
                <a>Gallery</a>
                <a>Terms</a>
                <a>Locations</a>
            </div>
            <div className="footer-container__content flex justify-content-between">
                <FooterMenu />
            </div>
            <div className="footer-container__footer">
                <div style={{ margin: "20px 0 30px" }}>
                    <div style={{ color: "#d7d7d7", marginBottom: "10px" }}>Delight customers everywhere with a branded custom-built native iOS, native Android and Installable Website Application.</div>
                    <div style={{ color: "#a5a5a5" }}>Opeqe is reliable, fast and commission free all-in-one ordering solutions for multi-location or single location restaurants.</div>
                </div>
                <div className="footer-container__footer__contact flex justify-content-between">
                    <div className="footer-container__footer__contact__info flex">
                        <div style={{paddingRight:"10px"}}>©2019 OPEQE INC</div> |
                        <a>Terms & Conditions</a>|
                        <a>Privacy Policy</a>
                    </div>
                    <div className="footer-container__footer__contact__icons flex">
                        <a><InstagramIcon /></a>
                        <a><TwitterIcon /></a>
                        <a><FacebookIcon /></a>
                        <a><YouTubeIcon /></a>
                    </div>
                </div>
            </div>
        </>
    )
}
