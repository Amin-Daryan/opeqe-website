import React from "react";

export default function FooterMenu() {
  return (
    <>
      <div className="flex justify-content-between" style={{flexWrap: "wrap"}}>
        <div className="flex-column menu-list">
          <span>Main Menu</span>
          <span>Pickup</span>
          <span>Delivery</span>
        </div>
        <div className="flex-column menu-list">
          <span>Orders</span>
          <span>Upcoming Orders</span>
          <span>Recent Orders</span>
        </div>
        <div className="flex-column menu-list">
          <span>Reservation</span>
          <span>Recent Reservation</span>
          <span>Wait To Be Seated</span>{" "}
        </div>
        <div className="flex-column menu-list">
          <span>Profile</span>
          <span>Promos & Credits</span>
          <span>Rewards</span>
        </div>
        <div className="flex-column menu-list">
          <span>Support</span>
          <span>Contact Us</span>
          <span>Live Chat</span>
        </div>
      </div>
      <div className="flex-column menu-list">
        <span>Special Offers</span>
        <span>Happy Hour (Egg Burger with Truffle Mayo)</span>
        <span>Happy Hour (California Chicken Salad)</span>
        <span>Happy Hour (Pad Thai)</span>
        <span>Happy Hour (Quarter Dark and Leg)</span>
      </div>
    </>
  );
}
