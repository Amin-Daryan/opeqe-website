import React, { useState, useEffect } from "react";
import Navbar from "./Navbar";
import Body from "./Body";
import Footer from "./Footer";

function App() {
  const [scrollTop, setScrollTop] = useState(true)

  function onScroll(e) {
    const newScrollTop = e.target.scrollTop
    console.log(e.target.scrollTop)
    setScrollTop(newScrollTop < 200)
  }

  useEffect(() => {
    document.body.addEventListener("scroll", onScroll)
  }, [])

  return (
    <div>
      <nav id="navbar-wrapper" className="navbar-container" style={{ backgroundColor: scrollTop ? "rgb(255, 210, 0)" : "white" }} >
        <Navbar scrollTop={scrollTop} />
      </nav>
      <div id="body-wrapper" className="body-container">
        <Body />
      </div>
      <footer id="footer-wrapper" className="footer-container">
        <Footer />
      </footer>
    </div >
  )
}

export default App;